package oop;

import java.util.HashSet;
import java.util.Objects;

public class Person {
    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        Person p = new Person("name", 33);
        Person p2 = new Person("name", 33);


        HashSet<Person> set  = new HashSet<>();
        set.add(p);
        System.out.println(set.contains(p2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
