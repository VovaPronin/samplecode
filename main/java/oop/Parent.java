package oop;


public class Parent {

    static {
        System.out.println("static parent");
    }

    {
        System.out.println("block parent");
    }

    public Parent() {
        System.out.println("parent constructor");
    }

    ParentReturn returnSome(){
        return null;
    }

    public static String doOne(){
        return "";
    }
}
