package oop;

public class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("new thread" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        new MyThread().start();
    }
}
