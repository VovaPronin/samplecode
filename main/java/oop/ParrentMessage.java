package oop;

public class ParrentMessage {
    public void call() {
        System.out.println("Parrent");
    }
}
class ChildMessage extends ParrentMessage{
    @Override
    public void call() {
        System.out.println("Child");
    }
}
class Test {

    public static void main(String[] args) {
printCall(new ChildMessage());
    }
    public static void printCall (ParrentMessage msg){
        msg.call();
    }
}