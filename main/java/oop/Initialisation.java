package oop;

public class Initialisation extends Parent {

    static String someValue = "some value";
    String another = "daf";

    static {
        System.out.println("static block " + someValue);
    }

    {
        System.out.println("block "  + another);
    }

    Initialisation() {
        super();
        System.out.println("constructor");
    }

    public static void main(String[] args) {
        Initialisation i = new Initialisation();
    }

}
