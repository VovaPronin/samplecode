package oop;

import java.util.ArrayList;

public class Child extends Parent {
    Parent parent;

    public final static String val = "";
    public final String dosomstr;


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Child){
            if(this == obj){
                return true;
            }
            return dosomstr.equals(((Child) obj).dosomstr);
        } else {
            return false;
        }
    }

    Child(){
        dosomstr = "";
    }

    @Override
    ChildReturn returnSome() {
        return null;
    }

    public static void main(String[] args) {
        SomeInterface.doSome();
        Object g = new Child();
        Parent p = new Child();
        Child c = new Child();

        Parent pr = c;
        Child newChild = (Child) p;

        Child parent = (Child) doSome(false);
        Child.doOne();
        Parent.doOne();
    }




    public static Parent doSome(boolean value) {
        if (value) {
            return new Parent();
        } else {
            return new Child();
        }
    }

    public static String doOne(){
        return "";
    }


    public void doSome2(){
        ArrayList str = new ArrayList<>();
        str.add("1");
        str.add("2");
        str.add("3");
        for (Object o : str) {
            if(o instanceof String){
                String val = (String) o;
                System.out.println(val);
            }
        }





    }
}
