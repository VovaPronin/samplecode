package nestedClasses;

public class TestInner01 {






    public static class Inner {
        private int k = 84;
        @Override
        public String toString() {
            return "Inner{" +
                    "k=" + k +
                    '}';
        }
        public void test2() {
            k++;
        }
    }
    public static void main(String[] args) {
        TestInner01 test = new TestInner01();
        TestInner01.Inner in = new Inner();
        TestInner01 t2 = new TestInner01();
        for (int i = 0; i < 20; i++) {
            in.test2();
        }
        System.out.println("result" + in);
    }
}

