package threadInterraptions;


public class Inter1 {
    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(() -> {
            while (true) {
                System.out.println(Thread.interrupted());
                for (int i = 0; i < 1_000_000L; i++) ;
            }
        });

        thread.start();
        thread.wait(1000);
        thread.interrupt();
    }
}