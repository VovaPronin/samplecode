package sample1;

public class NotifySamples {
    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = new Object();

        synchronized (o1) {
            synchronized (o1) {
                o1.notify();

            }
        }
        f2();
    }
    NotifySamples o3 = new NotifySamples();
    public synchronized void f(){
        this.notify();
    }
    public void f1(){
        synchronized(this){
            this.notify();
        }
    }
    public static synchronized void f2(){
        Class clazz = NotifySamples.class;
        synchronized (clazz) {
            clazz.notify();
        }


    }
}
