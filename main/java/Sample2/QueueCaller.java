package Sample2;

public class QueueCaller implements Runnable {
    private final BlockedQueue bq;
    private final int i;

    public QueueCaller(BlockedQueue bq, int i) {
        this.bq = bq;
        this.i = i;
    }



    @Override
    public void run() {
        try {
            bq.f(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
