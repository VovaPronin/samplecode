package contstructor;

public class Person {

    private String name;
    private String lastName;

    public Person(Person person){
        this.name = person.name;
        this.lastName = person.lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public static String getName(String str){
        return str;
    }

    public static void main(String[] args) {
        Person.getName("");
        Person per = new Person(null);
        per.getName("sgsffg");
    }
}
